/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.knight;

import aima.core.agent.Action;
import aima.core.agent.impl.DynamicAction;
import gyte.ai.eardic.knightwar.board.Square;
import java.awt.Point;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Emre
 */
public class Knight extends JLabel {

    // Actions of Knight
    public static final Action LeftUp = new DynamicAction("LeftUp");
    public static final Action LeftDown = new DynamicAction("LeftDown");

    public static final Action RightUp = new DynamicAction("RightUp");
    public static final Action RightDown = new DynamicAction("RightDown");

    public static final Action UpLeft = new DynamicAction("UpLeft");
    public static final Action UpRight = new DynamicAction("UpRight");

    public static final Action DownLeft = new DynamicAction("DownLeft");
    public static final Action DownRight = new DynamicAction("DownRight");

    public static final Action[] Actions = new Action[]{LeftUp, LeftDown, RightUp,
        RightDown, UpLeft, UpRight, DownLeft, DownRight};

    private static final long serialVersionUID = 1L;
    private Point position;
    private final Point goalPosition, initPosition;
    private final boolean black, user;
    // The last path knight followed
    private final Point[] path;

    public Knight(Point goal, Point init, boolean black) {
        this(goal, init, black, false);
    }

    public Knight(Point goal, Point init, boolean black, boolean user) {
        this.path = new Point[3];
        this.black = black;
        this.setVerticalAlignment(JLabel.CENTER);
        this.setHorizontalAlignment(JLabel.CENTER);
        this.position = init;
        this.goalPosition = goal;
        this.initPosition = init;
        this.user = user;
        // Set icon
        String iconName;
        if (black) {
            iconName = "knight_b.png";
        } else {
            iconName = "knight_w.png";
        }
        ImageIcon img = new ImageIcon(getClass().getClassLoader().getResource(
                "gyte/ai/eardic/knightwar/knight/resource/" + iconName));
        this.setIcon(img);
    }

    public Knight(Knight k) {
        this.path = new Point[3];
        this.setIcon(k.getIcon());
        this.setVerticalAlignment(JLabel.CENTER);
        this.setHorizontalAlignment(JLabel.CENTER);
        this.position = new Point(k.getPosition());
        this.goalPosition = k.getGoalPosition();
        this.initPosition = k.getInitPosition();
        this.black = k.isBlack();
        this.user = k.isUser();
    }

    public boolean isUser() {
        return user;
    }

    public boolean isBlack() {
        return black;
    }

    public void goInitialPosition() {
        this.position = this.initPosition;
    }

    public Point getGoalPosition() {
        return goalPosition;
    }

    public Point getInitPosition() {
        return initPosition;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public boolean isReachedGoal() {
        return position.equals(goalPosition);
    }

    public Point getNewPosition(Action a) {
        Point[] p = getPath(a);
        return p != null ? p[p.length - 1] : null;
    }

    public Point goNewPosition(Action a) {
        position = getNewPosition(a);
        return position;
    }

    public Point goNewPosition(Point p) {
        position = p;
        return position;
    }

    public Point goNewPosition(Square s) {
        position = s.getPosition();
        return position;
    }

    public Point[] getPath(Action a) {
        // LEFT MOVES ###############
        if (a == null) {
            return null;
        }
        if (a.equals(Knight.LeftUp)) {
            path[0] = new Point(position.x, position.y - 1);
            path[1] = new Point(position.x, position.y - 2);
            path[2] = new Point(position.x - 1, position.y - 2);
        } else if (a.equals(Knight.LeftDown)) {
            path[0] = new Point(position.x, position.y - 1);
            path[1] = new Point(position.x, position.y - 2);
            path[2] = new Point(position.x + 1, position.y - 2);
        } // RIGHT MOVES ###############
        else if (a.equals(Knight.RightUp)) {
            path[0] = new Point(position.x, position.y + 1);
            path[1] = new Point(position.x, position.y + 2);
            path[2] = new Point(position.x - 1, position.y + 2);
        } else if (a.equals(Knight.RightDown)) {
            path[0] = new Point(position.x, position.y + 1);
            path[1] = new Point(position.x, position.y + 2);
            path[2] = new Point(position.x + 1, position.y + 2);
        } // UP MOVES ###############
        else if (a.equals(Knight.UpRight)) {
            path[0] = new Point(position.x - 1, position.y);
            path[1] = new Point(position.x - 2, position.y);
            path[2] = new Point(position.x - 2, position.y + 1);
        } else if (a.equals(Knight.UpLeft)) {
            path[0] = new Point(position.x - 1, position.y);
            path[1] = new Point(position.x - 2, position.y);
            path[2] = new Point(position.x - 2, position.y - 1);
        } // DOWN MOVES ###############
        else if (a.equals(Knight.DownRight)) {
            path[0] = new Point(position.x + 1, position.y);
            path[1] = new Point(position.x + 2, position.y);
            path[2] = new Point(position.x + 2, position.y + 1);
        } else if (a.equals(Knight.DownLeft)) {
            path[0] = new Point(position.x + 1, position.y);
            path[1] = new Point(position.x + 2, position.y);
            path[2] = new Point(position.x + 2, position.y - 1);
        }

        return path;
    }

    @Override
    public String toString() {
        return black ? "Black" : "White";
    }

}
