/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.board;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * Describes A Square in Chess Board
 *
 * @author Emre
 */
@SuppressWarnings("FieldMayBeFinal")
public class Square extends JPanel {

    private static final Color BARRIER_COLOR = Color.orange;
    private static final Color AVAILABLE_COLOR = Color.green;
    private static final long serialVersionUID = 1L;

    private Point loc; // for row,column info
    private Color color; // the bg color of square

    public Square(Point location, Color col) {
        super(new BorderLayout());
        this.loc = location;
        this.color = col;
        setBackground(col);
        setBorder(BorderFactory.createEtchedBorder());
    }

    public Square(Square sq) {
        super(sq.getLayout());
        this.loc = sq.getPosition();
        this.color = sq.getColor();
        setBackground(color);
        setBorder(sq.getBorder());
    }

    public boolean isBlock() {
        return this.getBackground().equals(BARRIER_COLOR);
    }

    public void setBlock(boolean block) {
        if (block) {
            this.setBackground(BARRIER_COLOR);
        } else {
            this.setBackground(color);
        }
    }

    public void setAvailable(boolean ava) {
        if (ava) {
            this.setBackground(AVAILABLE_COLOR);
        } else {
            this.setBackground(color);
        }
    }

    public boolean isAvailable() {
        return this.getBackground().equals(AVAILABLE_COLOR);
    }

    public Point getPosition() {
        return loc;
    }

    public Color getColor() {
        return color;
    }

}
