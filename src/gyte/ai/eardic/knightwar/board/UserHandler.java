/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.board;

import gyte.ai.eardic.knightwar.ai.ChessKnightGame;
import gyte.ai.eardic.knightwar.gui.GOverDialog;
import gyte.ai.eardic.knightwar.knight.Knight;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * User move manager class
 *
 * @author Emre
 */
public class UserHandler implements MouseListener {

    private ChessBoard board;
    private Knight knightToMove;

    public UserHandler(ChessBoard board) {
        this.board = board;
        this.knightToMove = null;
    }

    public ChessBoard getBoard() {
        return board;
    }

    public void setBoard(ChessBoard board) {
        this.board = board;
    }

    /**
     * When user clicks a knight on board,the available squares to go will
     * appear for the knight
     *
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        Object src = e.getComponent();
        if (src instanceof Square) {
            Square clickedSquare = (Square) src;
            Knight knight = board.getKnightOnSquare(clickedSquare);

            if (knight != null && knight.isUser()
                    && board.getPlayerToMove().equals(knight)) {
                // If user click a knight on board
                // Get clicked knight
                // Show appliable actions on board for clicked knight
                board.showAppliableActions(knight, true);
                // Save clicked knight
                knightToMove = knight;
            } else if (clickedSquare.isAvailable() && knightToMove != null
                    && knightToMove.isUser()
                    && board.getPlayerToMove().equals(knightToMove)) {
                //If user clicks knight and selects appliable square
                //Hide green colors from squares
                board.showAppliableActions(knightToMove, false);
                //Remove knight from old position & Place knight to new position
                board.hideKnights();
                board.makeMove(clickedSquare.getPosition());
                board.showKnights();//Show knights as an image
                checkForWinner();
                // Reset clicked knight pointer
                knightToMove = null;
            } else if (knightToMove != null && knightToMove.isUser()) {
                // IF user clicks knight but doesn't select an appliable square
                board.showAppliableActions(knightToMove, false);
                knightToMove = null;
            }
        }
    }

    private void checkForWinner() {
        if (board.getUtility() != ChessKnightGame.NON_TERMINAL) {
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    GOverDialog d = new GOverDialog((JFrame)SwingUtilities.getRoot(board), board);
                    d.setLocationRelativeTo((JFrame)SwingUtilities.getRoot(board));
                    d.setVisible(true);
                }
            });
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
