/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.board;

import aima.core.agent.Action;
import gyte.ai.eardic.knightwar.ai.ChessKnightGame;
import gyte.ai.eardic.knightwar.knight.Knight;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author Emre
 */
public class ChessBoard extends JPanel {

    private static final long serialVersionUID = 1L;
    private int row, column; // row x column square board
    private Square[][] squares;
    private Knight blackKnight, whiteKnight;
    private int moveCount;
    private double utility;
    private UserHandler userHandler;

    /**
     * Initializes board using given row and column size
     *
     * @param row
     * @param column
     * @param barPer
     * @param whiteUser
     * @param blackUser
     */
    public ChessBoard(int row, int column, int barPer, boolean whiteUser, boolean blackUser) {
        super();
        // Init Knight
        whiteKnight = new Knight(new Point(row - 1, column - 1), new Point(0, 0), false, whiteUser);
        blackKnight = new Knight(new Point(0, 0), new Point(row - 1, column - 1), true, blackUser);
        initBoard(row, column, barPer);
        this.moveCount = 0;
        this.utility = ChessKnightGame.NON_TERMINAL;
        showKnights();
    }

    public ChessBoard(ChessBoard board) {
        copyBoard(board);
    }

    public void copyBoard(ChessBoard board) {
        // Shallow copy of board
        this.row = board.getRow();
        this.column = board.getColumn();

        setLayout(board.getLayout());
        setPreferredSize(board.getSize());
        setSize(board.getSize());
        setBounds(0, 0, board.getSize().width, board.getSize().height);
        this.squares = board.getSquares();

        // Deep copy - New Knight
        whiteKnight = new Knight(board.getWhiteKnight());
        blackKnight = new Knight(board.getBlackKnight());

        this.moveCount = board.getMoveCount();
        this.utility = board.getUtility();
        this.userHandler = board.getUserHandler();
        this.userHandler.setBoard(this);
    }

    private void initBoard(int row, int column, int per) {
        this.row = row;
        this.column = column;

        Dimension size = new Dimension(column * 50, row * 50);

        setLayout(new GridLayout(row, column));
        setPreferredSize(size);
        setSize(size);
        setBounds(0, 0, size.width, size.height);

        //Create squares of chess board
        userHandler = new UserHandler(this);
        squares = new Square[row][column];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < column; ++j) {
                squares[i][j] = new Square(new Point(i, j),
                        ((i + j) % 2 == 0) ? Color.GRAY : Color.WHITE);
                squares[i][j].addMouseListener(userHandler);
                add(squares[i][j]);
            }
        }

        addBarrierRandomly(per);
    }

    public UserHandler getUserHandler() {
        return userHandler;
    }

    public void resetBoard() {
        hideKnights();
        whiteKnight.goInitialPosition();
        blackKnight.goInitialPosition();
        showKnights();
        this.moveCount = 0;
        this.utility = ChessKnightGame.NON_TERMINAL;
        this.revalidate();
        this.repaint();
    }

    public void zoomOut() {
        Dimension size = getSize();
        size.width = (int) (size.width * 1.1);
        size.height = (int) (size.height * 1.1);
        setSize(size);
    }

    public void zoomIn() {
        Dimension size = getSize();
        size.width = (int) (size.width * 0.9);
        size.height = (int) (size.height * 0.9);
        setSize(size);
    }

    public void fitScreen(Dimension screen) {
        // Get small side of target
        int scr = screen.height;
        if (scr > screen.width) {
            scr = screen.width;
        }

        if (getSize().height > getSize().width) {
            while (getSize().height > scr) {
                zoomIn();
            }
        } else {
            while (getSize().width > scr) {
                zoomIn();
            }
        }
    }

    public Knight getPlayerToMove() {
        if (moveCount % 2 == 0) {
            return whiteKnight;
        }
        return blackKnight;
    }

    public void showAppliableActions(Knight k, boolean show) {
        if (getPlayerToMove().isUser()) {
            for (Action a : knightActions(k)) {
                Point avaPos = k.getNewPosition(a);
                getSquare(avaPos).setAvailable(show);
            }
        }
    }

    public int getMoveCount() {
        return moveCount;
    }

    public double getUtility() {
        return utility;
    }

    public Knight getBlackKnight() {
        return blackKnight;
    }

    public void setBlackKnight(Knight blackKnight) {
        this.blackKnight = blackKnight;
    }

    public Knight getWhiteKnight() {
        return whiteKnight;
    }

    public void setWhiteKnight(Knight whiteKnight) {
        this.whiteKnight = whiteKnight;
    }

    public Square[][] getSquares() {
        return squares;
    }

    public Square getSquare(Point p) {
        return squares[p.x][p.y];
    }

    public Square getSquare(int x, int y) {
        return squares[x][y];
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public void addBarrier(Point p) {
        squares[p.x][p.y].setBlock(true);
    }

    public boolean isBarrier(Point p) {
        return squares[p.x][p.y].isBlock();
    }

    public boolean inBoard(Point p) {
        return (p.x < row && p.x >= 0) && (p.y < column && p.y >= 0);
    }

    public boolean isKnight(Point p) {
        return (p.equals(whiteKnight.getPosition())
                || p.equals(blackKnight.getPosition()));
    }

    /**
     * Add barrier to board randomly
     *
     * @param percent is between 0-100
     */
    public void addBarrierRandomly(int percent) {
        Random rand = new Random();
        int barrierCount = (row * column - 2) * percent / 100;
        Point p = new Point();
        for (int i = 0; i < barrierCount; ++i) {
            do {
                p.x = rand.nextInt(row);
                p.y = rand.nextInt(column);
            } while (isBarrier(p) || isKnight(p));
            addBarrier(p);
        }
    }

    /**
     * Returns true if given action a is appliable for given knight
     *
     * @param knight
     * @param a
     * @return
     */
    private boolean canMoveKnight(Knight knight, Action a) {
        Point[] path = knight.getPath(a);
        if (path != null) {
            for (Point p : path) {
                if (!inBoard(p) || isBarrier(p) || isKnight(p)) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Applies action a to given knight
     *
     * @param knight
     * @param a
     */
    private void moveKnight(Knight knight, Action a) {
        if (getPlayerToMove().equals(knight)) {
            // Go to new square
            knight.goNewPosition(a);
            // Increment moveCount for nextPlayer to play
            ++moveCount;
            // Analyse utility value of current state
            analyseUtility();
        }
    }

    /**
     * Moves the knight to given point p
     *
     * @param playerToMove
     * @param p
     */
    private void moveKnight(Knight playerToMove, Point p) {
        if (getPlayerToMove().equals(playerToMove)) {
            playerToMove.goNewPosition(p);
            ++moveCount;
            analyseUtility();
        }
    }

    /**
     * Returns available actions for given knight
     *
     * @param k
     * @return
     */
    public List<Action> knightActions(Knight k) {
        List<Action> actions = new ArrayList<Action>();
        Action[] moves = Knight.Actions;
        for (Action a : moves) {
            if (canMoveKnight(k, a)) {
                actions.add(a);
            }
        }
        return actions;
    }

    /**
     * Applies the action a to next player
     *
     * @param a
     */
    public void makeMove(Action a) {
        moveKnight(getPlayerToMove(), a);
    }

    public void makeMove(Point p) {
        moveKnight(getPlayerToMove(), p);
    }
       
    /**
     * Show knights as an image on board
     */
    public void showKnights() {
        showKnight(whiteKnight);
        showKnight(blackKnight);
    }

    public void hideKnights() {
        hideKnight(whiteKnight);
        hideKnight(blackKnight);
    }

    private void hideKnight(Knight k) {
        k.setBorder(null);
        getSquare(k.getPosition()).remove(k);
        getSquare(k.getPosition()).revalidate();
        getSquare(k.getPosition()).repaint();
    }

    private void showKnight(Knight k) {
        getPlayerToMove().setBorder(BorderFactory.createLineBorder(Color.blue,2));
        getSquare(k.getPosition()).add(k);
        getSquare(k.getPosition()).revalidate();
        getSquare(k.getPosition()).repaint();
    }

    /**
     * Utility value of current state -1(non-term),1(win),0(lose),0.5(draw)
     */
    private void analyseUtility() {
        if (whiteKnight.isReachedGoal()) {
            utility = ChessKnightGame.WHITE_WIN;
        } else if (blackKnight.isReachedGoal()) {
            utility = ChessKnightGame.BLACK_WIN;
        } else if (isDraw()) {
            utility = ChessKnightGame.DRAW;
        } else {
            utility = ChessKnightGame.NON_TERMINAL;
        }
    }

    /**
     * Checks if current state is draw or not by examining available actions of
     * white and black knight If knight has no action to make, then the knight
     * is stuck and game is over
     *
     * @return
     */
    private boolean isDraw() {
        return knightActions(whiteKnight).isEmpty()
                || knightActions(blackKnight).isEmpty();
    }

    public Knight getKnightOnSquare(Square sq) {
        if (sq.getPosition().equals(whiteKnight.getPosition())) {
            return whiteKnight;
        } else if (sq.getPosition().equals(blackKnight.getPosition())) {
            return blackKnight;
        }
        return null;
    }

    @Override
    public String toString() {
        return "MoveC:" + moveCount + ",WK:[" + whiteKnight.getPosition().x + "," + whiteKnight.getPosition().y
                + "],BK:[" + blackKnight.getPosition().x + "," + blackKnight.getPosition().y + "],Util:" + utility;
    }

}
