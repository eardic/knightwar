/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.ai;

import aima.core.search.adversarial.AdversarialSearch;
import aima.core.search.framework.Metrics;

/**
 *
 * @author Emre
 * @param <STATE>
 * @param <ACTION>
 * @param <PLAYER>
 */
public abstract class AbstractCutOffSearch<STATE, ACTION, PLAYER> implements
        AdversarialSearch<STATE, ACTION> {

    protected final ImperfectGame<STATE, ACTION, PLAYER> game;
    protected int solutionDepth, maxDepth;
    protected int expandedNodes;

    public AbstractCutOffSearch(ImperfectGame<STATE, ACTION, PLAYER> game, int maxDepth) {
        this.game = game;
        this.solutionDepth = 0;
        this.expandedNodes = 0;
        this.maxDepth = maxDepth;
    }

    protected boolean isCutOff(int depth) {
        if (depth >= maxDepth) {
            solutionDepth = depth;
            return true;
        }
        return false;
    }

    @Override
    public abstract ACTION makeDecision(STATE state);

    @Override
    public Metrics getMetrics() {
        Metrics result = new Metrics();
        result.set("expandedNodes", expandedNodes);
        result.set("solutionDepth", solutionDepth);
        return result;
    }

}
