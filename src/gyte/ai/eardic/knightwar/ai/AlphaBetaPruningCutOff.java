/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.ai;

/**
 *
 * @author Emre
 * @param <STATE>
 * @param <ACTION>
 * @param <PLAYER>
 */
public class AlphaBetaPruningCutOff<STATE, ACTION, PLAYER> extends
        AbstractCutOffSearch<STATE, ACTION, PLAYER> {

    public AlphaBetaPruningCutOff(ImperfectGame game, int maxDepth) {
        super(game, maxDepth);
    }

    /**
     * Creates a new search object for a given game.
     *
     * @param <STATE>
     * @param <ACTION>
     * @param <PLAYER>
     * @param game
     * @param maxDepth
     * @return
     */
    public static <STATE, ACTION, PLAYER> AlphaBetaPruningCutOff<STATE, ACTION, PLAYER> createFor(
            ImperfectGame<STATE, ACTION, PLAYER> game, int maxDepth) {
        return new AlphaBetaPruningCutOff<STATE, ACTION, PLAYER>(game, maxDepth);
    }

    @Override
    public ACTION makeDecision(STATE state) {
        this.expandedNodes = 0;
        this.solutionDepth = 0;
        ACTION result = null;
        double resultValue = Double.NEGATIVE_INFINITY;
        PLAYER player = game.getPlayer(state);
        for (ACTION action : game.getActions(state)) {
            double value = minValue(game.getResult(state, action), player,
                    Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0);
            if (value > resultValue) {
                result = action;
                resultValue = value;
            }
        }
        return result;
    }

    public double maxValue(STATE state, PLAYER player, double alpha, double beta, int depth) {
        expandedNodes++;
        if (isCutOff(depth) || game.isTerminal(state)) {
            this.solutionDepth = depth;
            return game.getEval(state, player);
        }
        double value = Double.NEGATIVE_INFINITY;
        for (ACTION action : game.getActions(state)) {
            value = Math.max(value, minValue( //
                    game.getResult(state, action), player, alpha, beta, depth + 1));
            if (value >= beta) {
                return value;
            }
            alpha = Math.max(alpha, value);
        }
        return value;
    }

    public double minValue(STATE state, PLAYER player, double alpha, double beta, int depth) {
        expandedNodes++;
        if (isCutOff(depth) || game.isTerminal(state)) {
            this.solutionDepth = depth;
            return game.getEval(state, player);
        }
        double value = Double.POSITIVE_INFINITY;
        for (ACTION action : game.getActions(state)) {
            value = Math.min(value, maxValue( //
                    game.getResult(state, action), player, alpha, beta, depth + 1));
            if (value <= alpha) {
                return value;
            }
            beta = Math.min(beta, value);
        }
        return value;
    }

}
