/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.ai;

/**
 *
 * @author Emre
 * @param <STATE>
 * @param <ACTION>
 * @param <PLAYER>
 */
public class MiniMaxCutOff<STATE, ACTION, PLAYER> extends
        AbstractCutOffSearch<STATE, ACTION, PLAYER> {

    /**
     * Creates a new search object for a given game.
     *
     * @param <STATE>
     * @param <ACTION>
     * @param <PLAYER>
     * @param game
     * @param maxDepth
     * @return
     */
    public static <STATE, ACTION, PLAYER> MiniMaxCutOff<STATE, ACTION, PLAYER> createFor(
            ImperfectGame<STATE, ACTION, PLAYER> game, int maxDepth) {
        return new MiniMaxCutOff<STATE, ACTION, PLAYER>(game, maxDepth);
    }

    public MiniMaxCutOff(ImperfectGame<STATE, ACTION, PLAYER> game, int maxDepth) {
        super(game, maxDepth);
    }

    @Override
    public ACTION makeDecision(STATE state) {
        this.expandedNodes = 0;
        this.solutionDepth = 0;
        ACTION result = null;
        double resultValue = Double.NEGATIVE_INFINITY;
        PLAYER player = game.getPlayer(state);
        for (ACTION action : game.getActions(state)) {
            double value = minValue(game.getResult(state, action), player, 0);
            if (value > resultValue) {
                result = action;
                resultValue = value;
            }
        }
        return result;
    }

    public double maxValue(STATE state, PLAYER player, int depth) { // returns an utility
        // value
        expandedNodes++;
        if (isCutOff(depth) || game.isTerminal(state)) {
            this.solutionDepth = depth;
            return game.getEval(state, player);
        }
        double value = Double.NEGATIVE_INFINITY;
        for (ACTION action : game.getActions(state)) {
            value = Math.max(value,
                    minValue(game.getResult(state, action), player, depth + 1));
        }
        return value;
    }

    public double minValue(STATE state, PLAYER player, int depth) { // returns an utility
        // value
        expandedNodes++;
        if (isCutOff(depth) || game.isTerminal(state)) {
            this.solutionDepth = depth;
            return game.getEval(state, player);
        }
        double value = Double.POSITIVE_INFINITY;
        for (ACTION action : game.getActions(state)) {
            value = Math.min(value,
                    maxValue(game.getResult(state, action), player, depth + 1));
        }
        return value;
    }

}
