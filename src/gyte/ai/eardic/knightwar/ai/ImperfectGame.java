/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.ai;

import aima.core.search.adversarial.Game;

/**
 * 
 * @author Emre
 * @param <STATE>
 * @param <ACTION>
 * @param <PLAYER> 
 */
public interface ImperfectGame<STATE, ACTION, PLAYER> extends
        Game<STATE, ACTION, PLAYER> {

    public double getEval(STATE state, PLAYER player);

}
