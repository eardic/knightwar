/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyte.ai.eardic.knightwar.ai;

import aima.core.agent.Action;
import gyte.ai.eardic.knightwar.board.ChessBoard;
import gyte.ai.eardic.knightwar.knight.Knight;
import java.awt.Point;
import java.util.List;

/**
 *
 * @author Emre
 */
public class ChessKnightGame implements ImperfectGame<ChessBoard, Action, Knight> {

    public static final int NON_TERMINAL = -1, WHITE_WIN = Integer.MAX_VALUE,
            BLACK_WIN = Integer.MIN_VALUE, DRAW = 0;
    private ChessBoard initState = null;
    private Knight[] players = null;

    public ChessKnightGame(ChessBoard initState) {
        this.initState = initState;
        players = new Knight[2];
        players[0] = initState.getWhiteKnight();
        players[1] = initState.getBlackKnight();
    }

    @Override
    public ChessBoard getInitialState() {
        return initState;
    }

    @Override
    public Knight[] getPlayers() {
        return players;
    }

    @Override
    public Knight getPlayer(ChessBoard state) {
        return state.getPlayerToMove();
    }

    @Override
    public List<Action> getActions(ChessBoard state) {
        return state.knightActions(getPlayer(state));
    }

    @Override
    public ChessBoard getResult(ChessBoard state, Action action) {
        ChessBoard newBoard = new ChessBoard(state);
        newBoard.makeMove(action);
        return newBoard;
    }

    @Override
    public boolean isTerminal(ChessBoard state) {
        return state.getUtility() != NON_TERMINAL;
    }

    /**
     * Returns the utility value for player
     *
     * @param state
     * @param player
     * @return
     */
    @Override
    public double getUtility(ChessBoard state, Knight player) {
        double result = state.getUtility();//Utility for white knight
        if (result != NON_TERMINAL) {
            if (player.isBlack()) {// Utility of black is inverse of white
                result = -result;
            }
        } else {
            throw new IllegalArgumentException("State is not terminal.");
        }
        return result;
    }

    @Override
    public double getEval(ChessBoard state, Knight player) {

        if (isTerminal(state)) {
            return getUtility(state, player);
        }

        Knight white = state.getWhiteKnight();
        Knight black = state.getBlackKnight();

        double estM = estMove(white, black);
        double eucDist = eucDist(white, black);
        double mob = fMobility(state, white, black);
        double eval = estM + eucDist + mob / 4;

        if (player.isBlack()) {
            eval = -eval;
            /*System.out.println("Black" + ",State:" + state + ",Ev:" + eval
                    + "(" + estM + "," + eucDist + "," + mob / 4 + ")");*/

        } else {
            /*System.err.println("White" + ",State:" + state + ",Ev:" + eval
                    + "(" + estM + "," + eucDist + "," + mob / 4 + ")");*/

        }
        return eval;
    }

    /**
     * Player Dist - Opponent Dist If player is closer to its destination than
     * its opponent,then player is closer to victory
     *
     * @param player
     * @param opponent
     * @return
     */
    private double eucDist(Knight white, Knight black) {
        double whiteDiff = white.getGoalPosition().distance(white.getPosition());
        double blackDiff = black.getGoalPosition().distance(black.getPosition());
        return blackDiff - whiteDiff;
    }

    private double estMove(Knight white, Knight black) {
        double whiteDiff = Math.ceil(chebyshevDist(white.getGoalPosition(), white.getPosition()) / 2);
        double blackDiff = Math.ceil(chebyshevDist(black.getGoalPosition(), black.getPosition()) / 2);
        return blackDiff - whiteDiff;
    }

    private double chebyshevDist(Point p1, Point p2) {
        return Math.max(Math.abs(p1.x - p2.x), Math.abs(p1.y - p2.y));
    }

    /*private double manDist(Point p1, Point p2) {
     return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y);
     }*/
    /**
     * # of available moves for player - # of available moves for opponnet
     *
     * @param state
     * @param player
     * @param opponent
     * @return
     */
    private double fMobility(ChessBoard state, Knight white, Knight black) {
        return state.knightActions(white).size()
                - state.knightActions(black).size();
    }

}
